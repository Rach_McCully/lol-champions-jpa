package com.training.demo.service;

import com.training.demo.entity.LolChampion;
import com.training.demo.repository.LolChampionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {

    @Autowired
    LolChampionRepo lolChampionRepo;

    public List<LolChampion> findAll() {
        return this.lolChampionRepo.findAll();
    }

    public LolChampion LolChampion (LolChampion lolChampion) {
        return this.lolChampionRepo.save(lolChampion);
    }
}
