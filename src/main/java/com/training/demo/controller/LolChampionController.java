package com.training.demo.controller;

import com.training.demo.entity.LolChampion;
import com.training.demo.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@GetMapping("/api/v1/lolchampion")
public class LolChampionController {
    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return this.lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        System.out.println("Request to create lolChampion [" + lolChampion +"]");
        return this.lolChampionService.save(lolChampion);
    }
}
