package com.training.demo.repository;

import com.training.demo.entity.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepo extends JpaRepository<LolChampion, Long> {
}
